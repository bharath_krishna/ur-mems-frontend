import React, { Component, Fragment } from "react";
import { Form, Button } from "react-bootstrap";

class LoginPage extends Component {
  render() {
    var formGroupFields = [
      <Form.Group controlId={"loginError"} key={"loginError"}>
        {this.props.children}
      </Form.Group>
    ];
    ["username", "password"].forEach(field => {
      formGroupFields.push(
        <Form.Group controlId={field} key={field}>
          <Form.Label>Enter {field}</Form.Label>
          <Form.Control
            type={field !== "password" ? "text" : "password"}
            placeholder={field}
            onChange={this.props.onChange}
          />
        </Form.Group>
      );
    });
    return (
      <Fragment>
        <Form
          className="px-4 py-3"
          action="javascript:void(0)"
          onSubmit={this.props.Login}
        >
          {formGroupFields}
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </Fragment>
    );
  }
}

export default LoginPage;
