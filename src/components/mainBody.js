import React, { Component } from "react";
import SlideShow from "./slideShow";

import Axios from "axios";
import { Container, Row, Col, ListGroup } from "react-bootstrap";

export class MainContainer extends Component {
  state = {
    images: []
  };

  constructor() {
    super();
    // Set initial images
    this.state.images = [];
  }

  getImages = () => {
    return this.state.images;
  };
  updateImages = pageNo => {
    const headers = {
      Authorization: "Bearer " + this.props.cookies.get("userToken")
    };

    Axios({
      url: process.env.REACT_APP_API_URL + "/api/album/id/medias",
      method: "get",
      headers: headers
    })
      .then(results => {
        return results.data;
      })
      .then(props => {
        let images = props.Items.map(item => {
          return item.baseUrl;
        });
        this.setState({
          images: images
        });
      })
      .catch(e => {
        console.log(e);
        // window.location.reload();
      });
  };

  render() {
    return (
      <Container fluid>
        <Row>
          <Col sm={{ span: 2 }}>
            <Albums
              updateImages={this.updateImages.bind(this)}
              cookies={this.props.cookies}
            />
          </Col>
          <Col sm={{ span: 8 }}>
            <SlideShow
              getImages={this.getImages.bind(this)}
              cookies={this.props.cookies}
            />
          </Col>
          <Col sm={{ span: 2 }}>
            <Albums
              updateImages={this.updateImages.bind(this)}
              cookies={this.props.cookies}
            />
            {/* {RightPane()} */}
          </Col>
        </Row>
      </Container>
    );
  }
}

class Albums extends Component {
  AlbumLinks(albums) {
    const AlbumLinks = [];
    for (let i = 0; i < albums.length; i++) {
      AlbumLinks.push(
        <ListGroup.Item
          action
          // as={Button}
          key={albums[i]}
          onClick={() => {
            this.props.updateImages(i + 1);
          }}
        >
          {albums[i]}
        </ListGroup.Item>
      );
    }
    return AlbumLinks;
  }

  render() {
    const albumNames = getAlbumNames();
    return (
      <div>
        <h5>Albums</h5>
        <ListGroup>{this.AlbumLinks(albumNames)}</ListGroup>
      </div>
    );
  }
}

function getAlbumNames() {
  return ["hakone photos", "marriage photos", "fuji photos", "garden photos"];
}
