import React, { Component } from "react";

export default class SlideShow extends Component {
  state = {
    images: []
  };
  constructor(props) {
    super();
  }

  render() {
    return (
      <div
        id="carouselExampleIndicators"
        className="carousel slide"
        data-ride="carousel"
      >
        <ImagesIndicators images={this.props.getImages()} />
        <Images images={this.props.getImages()} />
        <ImagesControl action="prev" />
        <ImagesControl action="next" />
      </div>
    );
  }
}

function ImagesControl(props) {
  const acName = "carousel-control-" + props.action;
  const spancName = "carousel-control-" + props.action + "-icon";
  return (
    <React.Fragment>
      <a
        className={acName}
        href="#carouselExampleIndicators"
        role="button"
        data-slide={props.action}
      >
        <span className={spancName} aria-hidden="true" />
        <span className="sr-only">Previous</span>
      </a>
    </React.Fragment>
  );
}

function Images(props) {
  const imagesJsx = [];
  for (let i = 0; i < props.images.length; i++) {
    var corClass = "carousel-item";
    if (i === 0) {
      corClass += " active";
    }
    imagesJsx.push(
      <div className={corClass} key={i}>
        <img className="d-block w-100" src={props.images[i]} alt={i} />
      </div>
    );
  }
  return <div className="carousel-inner">{imagesJsx}</div>;
}

function ImagesIndicators(props) {
  const imagesList = [];
  for (let i = 0; i < props.images.length; i++) {
    var className = "";
    if (i === 0) {
      className += " active";
    }
    imagesList.push(
      <li
        key={i}
        data-target="#carouselExampleIndicators"
        data-slide-to={i}
        className={className}
      />
    );
  }
  return <ol className="carousel-indicators">{imagesList} </ol>;
}
