import React, { Component } from "react";
import NavBar from "./components/navBar";
import { MainContainer } from "./components/mainBody";
import LoginPage from "./components/loginPage";
import { Base64 } from "js-base64";
import Axios from "axios";
import { withCookies } from "react-cookie";
import { Alert, Container } from "react-bootstrap";
import { callApi } from "../src/utils.js";
import NavbarCollapse from "react-bootstrap/NavbarCollapse";

class App extends Component {
  state = {
    isLoggedIn: false,
    error: ""
  };
  componentWillMount() {
    // check user is logged in
  }
  componentDidMount() {
    // fetch images
  }
  onChange = e => {
    this.setState({ loginError: false });
    this.setState({ [e.target.id]: e.target.value });
  };
  LoginError(ErrorMessage) {
    var AlertComponent = "";
    if (ErrorMessage) {
      AlertComponent = (
        <Alert variant="danger sm">
          <strong>{ErrorMessage}</strong>
        </Alert>
      );
    } else {
      AlertComponent = (
        <Alert variant="info">
          <strong>Enter your username and password</strong>
        </Alert>
      );
    }
    return AlertComponent;
  }
  Login = e => {
    var headers = {
      Authorization:
        "Basic " +
        Base64.encode(this.state.username + ":" + this.state.password)
    };
    // let resp;
    // resp = callApi("post", "http://localhost:8088/authn/token", {}, headers);
    // console.log("this is resp:", resp);
    Axios({
      method: "post",
      url: process.env.REACT_APP_API_URL + "/api/authn/token",
      headers: headers
    })
      .then(results => {
        this.setState({
          userToken: results.data.access_token,
          isLoggedIn: true
        });
        this.props.cookies.set("userToken", this.state.userToken, {
          // path: "/"
        });
        // window.location.reload();
      })
      .catch(e => {
        this.setState({ loginError: true });
      });
  };

  render() {
    return (
      <div>
        <Container>
          {this.props.cookies.get("userToken") ? (
            <AppPage cookies={this.props.cookies} />
          ) : (
            <LoginPage
              cookies={this.props.cookies}
              Login={this.Login}
              onChange={this.onChange}
            >
              {this.state.loginError
                ? this.LoginError("Wrong username and password")
                : this.LoginError()}
            </LoginPage>
          )}
        </Container>
      </div>
    );
  }
}

class AppPage extends Component {
  Logout = () => {
    this.setState({
      isLoggedIn: false,
      userToken: ""
    });
    this.props.cookies.remove("userToken");
    // window.location.reload();
  };

  render() {
    return (
      <div>
        <NavBar Logout={this.Logout.bind(this)} cookies={this.props.cookies} />
        <MainContainer cookies={this.props.cookies} />
        {/*{RightPane()} */}
        {/* {Footer()} */}
      </div>
    );
  }
}

export default withCookies(App);
