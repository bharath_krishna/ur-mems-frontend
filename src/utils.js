import Axios from "axios";

export function callApi(method, url, body, headers) {
  console.log(method, url, headers);
  Axios({
    method: method,
    url: url,
    headers: headers
  })
    .then(results => {
      this.setState({
        userToken: results.data.access_token,
        isLoggedIn: true
      });
      // this.props.cookies.set("userToken", this.state.userToken, {
      //   // path: "/"
      // });
      //   window.location.reload();
    })
    .catch(e => {
      //   this.setState({ loginError: true });
      console.log(e.response);
    });

  return [];
}
